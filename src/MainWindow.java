import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.UIManager;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.proteanit.sql.DbUtils;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.ListSelectionModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

public class MainWindow {

	public JFrame mainFrame;
	private JTable tableWebPasswords;
	private JTable tableEmailPasswords;
	
	//Establish database connection
	Connection conn = dbConnection.dbConnector();

	//Create the application.
	public MainWindow() {
		
		initialize();
		RefreshTables();
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} 
		catch (Throwable e) {
			e.printStackTrace();
			}
		}

	//Initialize the contents of the frame.
	private void initialize() {
		
		DeleteFromDB delDB = new DeleteFromDB(); //To use the Delete(ID) function
		NewWebsite newWebsite = new NewWebsite(); //Declares the Add Website dialog window
		NewEmail newEmail = new NewEmail(); //Declares the Add Email dialog window
		
		//Main Frame declerations
		mainFrame = new JFrame();
		mainFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/icons/jPassLogo.png")));
		mainFrame.setFont(new Font("Verdana", Font.PLAIN, 16));
		mainFrame.setResizable(false);
		mainFrame.setTitle("jPass");
		mainFrame.setBounds(100, 100, 1072, 758);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.getContentPane().setLayout(null);
		
		//Tabbed Pane
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBorder(null);
		tabbedPane.setFont(new Font("Verdana", Font.PLAIN, 14));
		tabbedPane.setBounds(0, 0, 1066, 663);
		mainFrame.getContentPane().add(tabbedPane);
		
		//Website Tab
		JPanel tabWebsites = new JPanel();
		tabWebsites.setBorder(null);
		tabbedPane.addTab("Websites ", new ImageIcon(MainWindow.class.getResource("/icons/web.png")), tabWebsites, null);
		tabWebsites.setLayout(null);
		
		//Web password scroll pane
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(61, 11, 990, 595);
		tabWebsites.add(scrollPane);
		
		tableWebPasswords = new JTable();
		tableWebPasswords.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableWebPasswords.setFillsViewportHeight(true);
		tableWebPasswords.setFont(new Font("Verdana", Font.PLAIN, 17));
		tableWebPasswords.setRowHeight(35);
		tableWebPasswords.getTableHeader().setFont(new Font("Dialog", Font.BOLD,16));
		scrollPane.setViewportView(tableWebPasswords);
		
		//Website Tab toolbar
		JToolBar toolBar1 = new JToolBar();
		toolBar1.setFloatable(false);
		toolBar1.setOrientation(SwingConstants.VERTICAL);
		toolBar1.setRollover(true);
		toolBar1.setFont(new Font("Verdana", Font.PLAIN, 14));
		toolBar1.setBounds(10, 11, 41, 595);
		tabWebsites.add(toolBar1);
		
		//Add Website Button
		JButton addWebsiteButton = new JButton("");
		addWebsiteButton.setToolTipText("Add New Website Entry");
		toolBar1.add(addWebsiteButton);
		addWebsiteButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/add_list.png")));
		addWebsiteButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				//Opens new website dialog
				newWebsite.setVisible(true);
				newWebsite.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						RefreshTables();
						}
				});
			}
		});
		addWebsiteButton.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		//Websites Tab delete button
		JButton deleteButton1 = new JButton("");
		deleteButton1.setToolTipText("Delete Entry");
		toolBar1.add(deleteButton1);
		deleteButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Deletes entry on button press
				delDB.Delete("WebPasswords", tableWebPasswords.getSelectedRow()+1);
				RefreshTables();
				}
		});
		deleteButton1.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/delete.png")));
		deleteButton1.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		//Email Tab
		JPanel tabEmail = new JPanel();
		tabEmail.setLayout(null);
		tabbedPane.addTab("Email accounts ", new ImageIcon(MainWindow.class.getResource("/icons/email.png")), tabEmail, null);
		
		//Email Tab scroll pane
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(61, 11, 990, 595);
		tabEmail.add(scrollPane_1);
		
		tableEmailPasswords = new JTable();
		tableEmailPasswords.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableEmailPasswords.setFont(new Font("Verdana", Font.PLAIN, 17));
		tableEmailPasswords.setFillsViewportHeight(true);
		tableEmailPasswords.setRowHeight(35);
		tableEmailPasswords.getTableHeader().setFont(new Font("Dialog", Font.BOLD,16));
		scrollPane_1.setViewportView(tableEmailPasswords);
		
		//Email Tab toolbar
		JToolBar toolBar2 = new JToolBar();
		toolBar2.setOrientation(SwingConstants.VERTICAL);
		toolBar2.setFont(new Font("Verdana", Font.PLAIN, 14));
		toolBar2.setRollover(true);
		toolBar2.setFloatable(false);
		toolBar2.setBounds(10, 11, 41, 595);
		tabEmail.add(toolBar2);
		
		//Add new email adress button
		JButton addAddressButton = new JButton("");
		addAddressButton.setToolTipText("Add New Email Entry");
		toolBar2.add(addAddressButton);
		addAddressButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Opens new email dialog
				newEmail.setVisible(true);
				newEmail.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						RefreshTables();
						}
				});
			}
		});
		addAddressButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/add_list.png")));
		addAddressButton.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		//Email Tab delete button
		JButton deleteButton2 = new JButton("");
		deleteButton2.setToolTipText("Delete Entry");
		toolBar2.add(deleteButton2);
		deleteButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Deletes selected entry
				delDB.Delete("EmailPasswords", tableEmailPasswords.getSelectedRow()+1);
				RefreshTables();
				}
		});
		deleteButton2.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/delete.png")));
		deleteButton2.setFont(new Font("Verdana", Font.PLAIN, 14));
		
		//Exit Panel with "universal" Exit button
		JPanel exitPanel = new JPanel();
		exitPanel.setBounds(0, 664, 1066, 65);
		mainFrame.getContentPane().add(exitPanel);
		exitPanel.setLayout(null);
		
		//Exit Button
		JButton exitButton = new JButton("Exit");
		exitButton.setToolTipText("Close the application");
		exitButton.setBounds(943, 11, 113, 43);
		exitPanel.add(exitButton);
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//Closes main window
				mainFrame.dispose();
				}
		});
		exitButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/exit.png")));
		exitButton.setFont(new Font("Verdana", Font.PLAIN, 14));

	}
	
	public void RefreshTables(){
		//This method is for refreshing the app's tables using the rs2xml.jar library
		
		try{
			String query = "select * from WebPasswords;";
			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			tableWebPasswords.setModel(DbUtils.resultSetToTableModel(rs));
			
			query = "select * from EmailPasswords;";                   
			pst = conn.prepareStatement(query); 
			rs = pst.executeQuery();   
			tableEmailPasswords.setModel(DbUtils.resultSetToTableModel(rs));
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
}
