import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import net.proteanit.sql.DbUtils;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.DropMode;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Dialog.ModalityType;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JPasswordField;

@SuppressWarnings({ "unused", "serial" })
public class NewWebsite extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField websiteName;
	private JTextField userName;
	private JTextField note;
	private JTextField passWord;
	
	String website = null; 
	String user = null;
	String pass = null;
	String n = null;	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		try {
			NewWebsite dialog = new NewWebsite();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	@SuppressWarnings("rawtypes")
	public NewWebsite() {

		setAlwaysOnTop(true);

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setIconImage(Toolkit.getDefaultToolkit().getImage(NewWebsite.class.getResource("/icons/add_list.png")));
		
		InsertIntoDB insDB = new InsertIntoDB(); //To use the Insert(ID, varToInsert) function
		
		setFont(new Font("Dialog", Font.PLAIN, 14));
		setTitle("Add new website");
		setResizable(false);
		getContentPane().setFont(new Font("Verdana", Font.PLAIN, 14));
		setBounds(100, 100, 360, 237);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		websiteName = new JTextField(website);
		websiteName.setFont(new Font("Verdana", Font.PLAIN, 14));
		websiteName.setColumns(10);
		websiteName.setBounds(108, 11, 233, 27);
		contentPanel.add(websiteName);
		
		userName = new JTextField(user);
		userName.setFont(new Font("Verdana", Font.PLAIN, 14));
		userName.setColumns(10);
		userName.setBounds(108, 49, 233, 27);
		contentPanel.add(userName);
		
		passWord = new JTextField(pass);
		passWord.setToolTipText("");
		passWord.setFont(new Font("Verdana", Font.PLAIN, 14));
		passWord.setColumns(10);
		passWord.setBounds(108, 87, 233, 27);
		contentPanel.add(passWord);
		
		note = new JTextField(n);
		note.setToolTipText("If applicable");
		note.setFont(new Font("Verdana", Font.PLAIN, 14));
		note.setColumns(10);
		note.setBounds(108, 125, 233, 27);
		contentPanel.add(note);
		
		JLabel lblWebsite = new JLabel("Website:");
		lblWebsite.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblWebsite.setBounds(10, 11, 77, 27);
		contentPanel.add(lblWebsite);
		
		JLabel lblUsername = new JLabel("UserName:");
		lblUsername.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblUsername.setBounds(10, 49, 77, 27);
		contentPanel.add(lblUsername);
		
		JLabel lblPassWord = new JLabel("PassWord:");
		lblPassWord.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblPassWord.setBounds(10, 87, 77, 27);
		contentPanel.add(lblPassWord);
		
		JLabel lblNote = new JLabel("Note:");
		lblNote.setFont(new Font("Verdana", Font.PLAIN, 14));
		lblNote.setBounds(10, 125, 77, 27);
		contentPanel.add(lblNote);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						website = websiteName.getText();
						user = userName.getText();
						pass = passWord.getText();
						n = note.getText();
						
						insDB.Insert("WebPasswords", website, user, pass, n);
						
						dispose();
						websiteName.setText(""); //Clear text fields when Dialog closes (this and below 5 lines)
						userName.setText("");
						passWord.setText("");
						note.setText("");
	
					}
				});
				okButton.setFont(new Font("Verdana", Font.PLAIN, 14));
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						
						dispose();
						websiteName.setText(""); //Clear text fields when Dialog closes (this and below 5 lines)
						userName.setText("");
						passWord.setText("");
						note.setText("");
					}
				});
				cancelButton.setFont(new Font("Verdana", Font.PLAIN, 14));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
